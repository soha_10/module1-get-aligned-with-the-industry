# Product 1

## Product name

**Deep Instinct**

### Product link 

[Click here](https://www.deepinstinct.com/)

### Product short description(*what the product is about*)

Threat Prevention platform using Deep Learning, based upon:
- prevention first approach
- detection & response
- automatic analysis
- remediation
Technology partnership: AWS, IBM, VMware, citrix etc.

### Product is a combination of features 

✓  Object detection
  
✓  ID Recognition

✓  Person Detection

### Product is provided by

Private company founded in 2014 by Caspi, Dr. Eli David, and Nadav Maman. The Headquarters are situated in New York.


# Product 2

## Product name

**NVISO**

### Product link

[Click here](https://www.nviso.ai/en)

### Product short description (*what the product is about*)

To accurately detect and predict human behaviors using visual intelligence, turning this intelligence into business solution in the fields of Financ, Automative, Healthcare and Robotics. 

### Product is a combination of features

✓ Object detection

✓ Pose Estimations

✓ ID Recognition

✓ Person Detection

### Product is provided by

Tim Llewellynn  CEO & Co-founder; Matteo Sorci  CTO & Co-founder.


# Product 3

## Product name

**NIRAMAI**

### Product link

[Click here](https://www.niramai.com/)

### Product short description (*what the product is about*)

NIRAMAI - "Non-Invasive Risk Assessment with Machine Intelligence"
Novel software to detect breast cancer.
Used for regular preventive health checkups, and also for large scale screening in rural  and semi-urban areas.

### Product is a combination of features

Thermal image proceesing and machine learning algorithms.

### Product is provided by

Dr. Geetha Manjunath is the Founder, CEO and CTO of Niramai, a deep tech startup.


# Product 4

## Product name

**Doxper**

### Product link

[Click here](https://doxper.com/home/)

### Product short description (*what the product is about*)

Doxper is a digital pen and encoded paper-based clinical documentation system powered by cloud computing, machine learning and AI. Doxper provides seamless clinical documentation with no behaviour change and no more burnout. 

### Product is a combination of features

✓ Segmentation

✓ Machine learning algorithms

### Product is provided by

Created by Shailesh Prithani, Pawan Jain and Randeep Singh. 

